exec { 'apt-get update':
  path => '/usr/bin',
}

package { 'vim':
	ensure => 'installed'
}

package { 'git':
	ensure => 'installed'
}

package { 'curl':
	ensure => 'installed'	
}

file { '/var/www/':
  ensure => 'directory',
}

include nginx, php, stdlib, wget

class { '::mysql::server':
  root_password           => '123456',
  remove_default_accounts => false,
   override_options => {
        mysqld => {
            "bind_address"  => "0.0.0.0",
        }
      }
}

mysql_grant { 'root@localhost/*.*':
  ensure     => 'present',
  options    => ['GRANT'],
  privileges => ['ALL'],
  table      => '*.*',
  user       => 'root@localhost',
}

class { 'nodejs':
  version    => 'stable',
  target_dir => '/bin',
}


